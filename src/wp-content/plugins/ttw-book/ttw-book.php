<?php 
/*
Plugin Name: TTW-Book
Plugin URI: https://flower2502.000webhostapp.com/
Description: TTW Book Plugin
Version: 1.0.0
Author: Tu Tu
Author URI: https://flower2502.000webhostapp.com/
License: GPLv2 or later
Text Domain: ttw_call
*/

define('TTW_BOOK_PLUGIN_DIR', plugin_dir_path(__FILE__));

//register_activation_hook(__FILE__, 'ttw_book_active');


function ttw_book_active() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'ttw_book';

    if($wpdb->get_var("SHOW TABLES LIKE '" .$table_name."'") != $table_name) {
        $sql = "CREATE TABLE `".$table_name."` ( `id` INT NULL AUTO_INCREMENT , `name` VARCHAR(50) NOT NULL, PRIMARY KEY (`id`))
        ENGINE = InnoDB CHARSET=utf8 COLLATE utf8_general_ci;";
        require_once ABSPATH . 'wp_admin/includes/upgrade.php';
        $dbDelta($sql);
    }
    // $ttw_book_version = '1.0';
    // add_option('_ttw_book_version', $ttw_book_version, '', 'yes');
} 

register_deactivation_hook(__FILE__, 'ttw_book_deactive');

function ttw_book_deactive() {
    global $wpdb;
    $table_name = $wpdb->prefix . 'options';
    $wpdb->update(
        $table_name,
        array('autoload'=>'no'),
        array('option_name'=>'_ttw_book_version'),
        array('%s'),
        array('%s'),
    );
}

register_uninstall_hook(__FILE__, 'ttw_book_uninstall');

function ttw_book_uninstall() {
    global $wpdb;
    delete_option('_ttw_book_version');
    $table_name = $wpdb->prefix . 'ttw_book';
    $sql = 'DROP TABLE IF EXISTS ' . $table_name;
    $wpdb->query($sql);
}

// add_action('admin_menu', 'ttw_book_setting');

// function ttw_book_setting() {
//     add_menu_page(
//         "TTW Book",
//         "TTW Book",
//         "manage_options",
//         "ttw-book",
//         "ttw_setting_book",
//         'dashicons-products',
//     );
// }

function custom_post_type_book() {
  
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Books', 'Post Type General Name', 'tutuweb' ),
            'singular_name'       => _x( 'Book', 'Post Type Singular Name', 'tutuweb' ),
            'menu_name'           => __( 'Books', 'tutuweb' ),
            'parent_item_colon'   => __( 'Parent Book', 'tutuweb' ),
            'all_items'           => __( 'All Books', 'tutuweb' ),
            'view_item'           => __( 'View Book', 'tutuweb' ),
            'add_new_item'        => __( 'Add New Book', 'tutuweb' ),
            'add_new'             => __( 'Add New', 'tutuweb' ),
            'edit_item'           => __( 'Edit Book', 'tutuweb' ),
            'update_item'         => __( 'Update Book', 'tutuweb' ),
            'search_items'        => __( 'Search Book', 'tutuweb' ),
            'not_found'           => __( 'Not Found', 'tutuweb' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'tutuweb' ),
        );
          
    // Set other options for Custom Post Type
          
        $args = array(
            'label'               => __( 'Books', 'tutuweb' ),
            'description'         => __( 'Book news and reviews', 'tutuweb' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'post-formats' ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            // 'taxonomies'          => array( 'books', 'loai-san-pham' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-products',
            'rewrite' => array( 'slug' => 'books')
      
        );
          
        // Registering your Custom Post Type
        register_post_type( 'books', $args );
      
    }
      
add_action( 'init', 'custom_post_type_book', 0 );

function ttw_setting_book() {
    require(TTW_BOOK_PLUGIN_DIR . 'ttw-book-setting.php');
}

function display_ttw_book() {
    // $number_phone = get_option("number_phone");
    require(TTW_BOOK_PLUGIN_DIR . 'ttw-book-view.php');
}

//add_action('wp_head', 'display_ttw_book');