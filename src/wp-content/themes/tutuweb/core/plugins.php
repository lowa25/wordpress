<?php
    function tutuweb_plugin_activation() {
        /* declare plugins need to install */
        $plugins = array(
            array(
                'name' => 'Redux Framework',
                'slug' => 'redux-framework',
                'required' => true
            )
        );

        /* Init TGM */
        $configs = array(
            'menu' => 'tp_plugin_intall',
            'has_notice' => true,
            'dismissable' =>false,
            'is_automatic' => true
        );
        tgmpa( $plugins, $configs );
    }
    add_action('tgmpa_register', 'tutuweb_plugin_activation');
?>