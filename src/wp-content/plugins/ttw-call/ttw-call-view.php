<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">

<a href="<?php echo $number_phone; ?>" id="alo-phoneIcon" data-toggle="modal" data-target="#callme-modal" class="alo-phone alo-green alo-show">
    <div class="alo-ph-circle"></div>
    <div class="alo-ph-circle-fill"></div>
    <div class="alo-ph-img-circle"><i class="fa fa-phone"></i></div><span class="alo-ph-text"><?php echo $number_phone; ?></span>
</a>