<form method="post">
    <p>
        <label>Name (*)</label> <br>
        <input type="text" name="name" id="name" value="" />
    </p>

    <p>
        <label>Phone (*)</label> <br>
        <input type="text" name="phone" id="phone" value="" />
    </p>

    <p>
        <label>Email (*)</label> <br>
        <input type="email" name="email" id="email" value="" />
    </p>

    <p>
        <label>Content</label> <br>
        <textarea rows="10" cols="75" name="content"></textarea>
    </p>

    <p>
        <input id="submit" type="submit" name="submit" class="button button-custom" value="Submit" />
    </p>
</form>