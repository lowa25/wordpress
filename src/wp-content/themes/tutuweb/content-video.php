<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
    <div class="entry-header">
        <?php tutuweb_entry_header(); ?>
        <?php tutuweb_entry_meta(); ?>
    </div>
    <div class="entry-content">
        <?php the_content_video(); ?>
        <?php ( is_single() ? tutuweb_entry_tag() : '' ); ?>
    </div>
    <?php 
    ?>
</article>
