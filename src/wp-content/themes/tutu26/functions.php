<?php

/* 
    Define const
    @THEME : get url folder theme
    $CORE : get url folder core
*/

define ( 'THEME_URL', get_stylesheet_directory() );
define( 'IMG_PATH' , get_template_directory_uri().'/assets/img');


if( !function_exists('tutu26_theme_setup') ) {
    function  tutu26_theme_setup() {

        /* Init Textdomain */
        $language_folder = THEME_URL . '/languages';
        load_theme_textdomain( 'tutu26', $language_folder );

        /* Import auto link RSS to head*/
        add_theme_support( 'automatic-feed-links' );

        /* Theme post thumbnail */
        add_theme_support( 'post-thumbnails' );

        /* Post Format */
        add_theme_support( 'post-formats', array(
            'image',
            'gallery',
            'quote',
            'link',
            'video',
        ) );

        /* Add title-tag */
        add_theme_support( 'title-tag' );
           
        /* Custom background*/
        $default_background = array(
            'default-color' => '#e8e8e8'
        );
        add_theme_support( 'custom-background', $default_background );

        /* Add menu */
        register_nav_menu( 'main_menu', __( 'Main Menu', 'tutu26') );

    }
    add_action ( 'init', 'tutu26_theme_setup' );
}

/* Import css */

function tutu26_style() {
    wp_register_style( 'main-style', get_template_directory_uri() ."/style.css", 'all' );
    wp_enqueue_style( 'main-style' );

}

add_action('wp_enqueue_scripts', 'tutu26_style');

if ( ! function_exists( 'get_menu_header' ) ) :

	function get_menu_header($name) {

		$menuLocations = get_nav_menu_locations();
		$navbar_items = wp_get_nav_menu_items($menuLocations[$name]);
		$child_items = [];

		foreach ($navbar_items as $key => $item) {
			if ($item->menu_item_parent) {
				array_push($child_items, $item);
				unset($navbar_items[$key]);
			}
		}

		foreach ($navbar_items as $item) {
			foreach ($child_items as $key => $child) {
				if ($child->menu_item_parent == $item->ID) {
					if (!$item->child_items) {
						$item->child_items = [];
					}

					array_push($item->child_items, $child);

					unset($child_items[$key]);
				}
			}
		}

		return $navbar_items;
	}

endif;

function custom_post_type_scenes() {
  
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Scene', 'Post Type General Name', 'tutu26' ),
            'singular_name'       => _x( 'Scene', 'Post Type Singular Name', 'tutu26' ),
            'menu_name'           => __( 'Scenes', 'tutu26' ),
            'parent_item_colon'   => __( 'Parent Scene', 'tutu26' ),
            'all_items'           => __( 'All Scene', 'tutu26' ),
            'view_item'           => __( 'View Scene', 'tutu26' ),
            'add_new_item'        => __( 'Add New Scene', 'tutu26' ),
            'add_new'             => __( 'Add New', 'tutu26' ),
            'edit_item'           => __( 'Edit Scene', 'tutu26' ),
            'update_item'         => __( 'Update Scene', 'tutu26' ),
            'search_items'        => __( 'Search Scene', 'tutu26' ),
            'not_found'           => __( 'Not Found', 'tutu26' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'tutu26' ),
        );
          
    // Set other options for Custom Post Type
          
        $args = array(
            'label'               => __( 'Scenes', 'tutu26' ),
            'description'         => __( 'Scene news and reviews', 'tutu26' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'post-formats' ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'taxonomies'          => array( 'Category', 'Tags' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-images-alt',
            'rewrite' => array( 'slug' => 'scenes')
      
        );
          
        // Registering your Custom Post Type
        register_post_type( 'scenes', $args );
      
    }
      
add_action( 'init', 'custom_post_type_scenes', 0 );


function create_scene_category_taxonomy() {
	$labels = array(
		'name' => 'Category',
		'singular' => 'Scene Category',
		'menu_name' => 'Category'
	);


	$args = array(
		'labels'                     => $labels,
		'hierarchical'             => true,
		'public'                   => true,
		'show_ui'                   => true,
		'show_admin_column'       => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'            => true,
	);


    register_taxonomy( 'scene_category', 'scenes' , $args );

}

add_action( 'init', 'create_scene_category_taxonomy', 0 );

function create_tags_taxonomy() {

	$labels = array(
		'name' => 'Tags',
		'singular' => 'Tags',
		'menu_name' => 'Tags',
        'all_items' => 'All Tags',
        'parent_item' => 'Parent Tags',
        'new_item_name' => 'New Tags',
        'add_new_item' => 'Add New Tags',
        'edit_item' => 'Edit Tags',
        'update_item' => 'Update Tags',
        'search_items' => 'Search Tags',
        'add_or_remove_items' => 'Add or Remove Tags',
	);


	$args = array(
		'labels'                     => $labels,
		'hierarchical'             => true,
		'public'                   => true,
		'show_ui'                   => true,
		'show_admin_column'       => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'            => true,
	);

    register_taxonomy( 'scene_tags', array( 'scenes' ), $args );

}



add_action( 'init', 'create_tags_taxonomy', 0 );

/* Pagination */
if ( !function_exists( 'tutu26_pagination' )) {
    function tutu26_pagination($paged, $total, $args = array()) {
        $paginationHTML = ' <div class="row"><div class="col s12"><ul class="pagination custom-pagination">';
            $pagination = paginate_links( array(
                'base'         => str_replace( 999999999, '%#%', html_entity_decode( get_pagenum_link( 999999999 ) ) ),
                'total'        => $total,
                'current'      => $paged,
                'format'       => '?paged=%#%',
                'show_all'     => false,
                'type'         => 'array',
                'end_size'     => 2,
                'mid_size'     => 1,
                'prev_next'    => true,
                'prev_text'    => '<i class="material-icons">chevron_left</i>',
                'next_text'    => '<i class="material-icons">chevron_right</i>',
                'add_args'     => $args,
                'add_fragment' => '',
            ) );
           
           
            if (!empty($pagination)):
                foreach($pagination as $key => $page){
                    $paginationHTML .= '<li class="waves-effect">'.$page.'</li>';
                }
            endif;
            return $paginationHTML .= ' </ul></div></div>';
    }
}

?>