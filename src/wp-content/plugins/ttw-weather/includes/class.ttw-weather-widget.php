<?php
/**
 * @package TTW-Weather
 */
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class TTW_Weather_Widget extends WP_Widget {

    function __construct() {
		parent::__construct(
			'ttw_weather_widget',
			esc_html__( 'TTW Weather Widget', 'ttw_weather' ),
			array( 'description' => esc_html__( 'Simple Weather Widget', 'ttw_weather' ), ) 
		);

		add_action('wp_enqueue_scripts', function() {
			wp_register_style('ttw-css', TTW_WEATHER_PLUGIN_URL . 'scripts/css/style.css');
			wp_enqueue_style('ttw-css');

			wp_register_script( 'ttw-js', TTW_WEATHER_PLUGIN_URL . "scripts/js/functions.js",  array('jquery'));
            wp_localize_script('ttw-js', 'ttw', [
                'url' => admin_url('admin-ajax.php'),
            ]);
            wp_enqueue_script('ttw-js');
		});
	}
    
    function form($instance) {
		$title = (isset($instance['title']) && !empty($instance['title'])) ? apply_filters('widget_title', $instance['title']) : __('TTW Weather Widget', 'ttw_weather');
		$unit = (isset($instance['unit']) && !empty($instance['unit'])) ? $instance['unit'] : 'celsius';
        require(TTW_WEATHER_PLUGIN_DIR . 'views/ttw-weather-widget-form.php');
    }

    function update($new_instance, $old_instance) {
		$instance = [];
		$instance['title'] = (isset($new_instance['title']) && !empty($new_instance['title'])) ? apply_filters('widget_title', $new_instance['title']) : __('TTW Weather Widget', 'ttw_weather');
		$instance['unit'] =  (isset($new_instance['unit']) && !empty($new_instance['unit'])) ? $new_instance['unit'] : 'celsius';
		return $instance;
    }

    function widget($args, $instance) {
		$title = (isset($instance['title']) && !empty($instance['title'])) ? apply_filters('widget_title', $instance['title']) : __('TTW Weather Widget', 'ttw_weather');
		if(get_option('ttw_weather_setting')) {
			$city_name = get_option('ttw_weather_setting')['city_name'];
		} else {
			$city_name = 'Ho+Chi+Minh';
		}
		$widget_option = $instance;
		$data = TTW_Weather_API::get_weather($city_name);
		require(TTW_WEATHER_PLUGIN_DIR . 'views/ttw-weather-widget-view.php');
    }
}

function ttw_widget_register_widgets() {
	register_widget( 'TTW_Weather_Widget' );
}
add_action( 'widgets_init', 'ttw_widget_register_widgets' );