<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Camtu
 * @subpackage Plugin_Camtu/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div id="wrap">
	<form method="post" action="options.php">
		<?php
			settings_fields( 'plugin-camtu-settings' );
			do_settings_sections( 'plugin-camtu-settings' );
			submit_button();
		?>
	</form>
</div>