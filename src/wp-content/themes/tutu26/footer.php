 <!-- jQuery Library -->
 <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <!-- Materialize js -->
    <script type="text/javascript" src="<?php bloginfo('template_directory');?>/assets/js/materialize.min.js"></script>
    <!-- Skill Progress Bar -->
    <script src="<?php bloginfo('template_directory');?>/assets/js/appear.min.js" type="text/javascript"></script>
    <script src="<?php bloginfo('template_directory');?>/assets/js/pro-bars.min.js" type="text/javascript"></script>
    <!-- Owl slider -->      
    <script src="<?php bloginfo('template_directory');?>/assets/js/owl.carousel.min.js"></script>    
    <!-- Mixit slider  -->
    <script src="http://cdn.jsdelivr.net/jquery.mixitup/latest/jquery.mixitup.min.js"></script>
    <!-- counter -->
    <script src="<?php bloginfo('template_directory');?>/assets/js/waypoints.min.js"></script>
    <script src="<?php bloginfo('template_directory');?>/assets/js/jquery.counterup.min.js"></script>     

    <!-- Custom Js -->
    <script src="<?php bloginfo('template_directory');?>/assets/js/custom.js"></script>      

     <!-- bootstrap Js -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-HwwvtgBNo3bZJJLYd8oVXjrBZt8cqVSpeBNS5n7C8IVInixGAoxmnlMuBnhbgrkm" crossorigin="anonymous"></script> -->

    <?php wp_footer(); ?>
</body>
</html>