<?php
/* 
    Define const
    @THEME : get url folder theme
    $CORE : get url folder core
*/

define ( 'THEME_URL', get_stylesheet_directory() );
define ( 'CORE', THEME_URL . '/core' );

// Custom comment walker.
require get_template_directory() . '/classes/tutuweb-walker-comment.php';

/* 
    Import file core/init.php
*/

require_once (CORE . '/init.php');

/* 
    Format width content 
*/

if( !isset($content_width) ) {
    $content_width = 620;
}

/* 
    Declare function for theme
*/

if( !function_exists('tutuweb_theme_setup') ) {
    function  tutuweb_theme_setup() {

        /* Init Textdomain */
        $language_folder = THEME_URL . '/languages';
        load_theme_textdomain( 'tutuweb', $language_folder );

        /* Import auto link RSS to head*/
        add_theme_support( 'automatic-feed-links' );

        /* Theme post thumbnail */
        add_theme_support( 'post-thumbnails' );

        /* Post Format */
        add_theme_support( 'post-formats', array(
            'image',
            'gallery',
            'quote',
            'link',
            'video',
        ) );

        /* Add title-tag */
        add_theme_support( 'title-tag' );
           
        /* Custom background*/
        $default_background = array(
            'default-color' => '#e8e8e8'
        );
        add_theme_support( 'custom-background', $default_background );

        /* Add menu */
        register_nav_menu( 'primary-menu', __( 'Primary Menu', 'tutuweb') );

        /* Add Sidebar */
        $sidebar = array(
            'name' => __( 'Main Sidebar', 'tutuweb' ),
            'id' => 'main-sidebar',
            'description' => __( 'Default Sidebar' ),
            'class' => 'main-sidebar',
            'before_title' => '<h3 class="widgettitle">',
            'after_title' => '</h3>'
        );

        register_sidebar( $sidebar );

        
    }
    add_action ( 'init', 'tutuweb_theme_setup' );
}


/* Template Header */
if ( !function_exists ( 'tutuweb_header' )) {
    function tutuweb_header() { 
            $currentLanguage = qtranxf_getLanguage();

            $availableLanguagesList = qtranxf_getSortedLanguages();

        ?>
        <div class="container">
            <div class="row site-name">
                <div class="col">
                    <?php
                        if( is_home()) {
                            printf( '<h1><a href="%1$s" title="%2$s">%3$s</a></h1>' ,
                            get_bloginfo( 'url' ),
                            get_bloginfo( 'description' ),
                            get_bloginfo( 'sitename' ) );
                        } else {
                            printf( '<p><a href="%1$s" title="%2$s">%3$s</a></p>',
                            get_bloginfo( 'url' ),
                            get_bloginfo( 'description' ),
                            get_bloginfo( 'sitename' ) );
                        }
                    ?>
                </div>
                <div class="col justify-content-end d-flex">
                    <div class="btn-group">
                      <button class="btn btn-secondary btn-sm dropdown-toggle button-lang" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <?= ucfirst($currentLanguage) ?>
                      </button>
                      <div class="dropdown-menu">
                            <?php
                                foreach($availableLanguagesList as $lang) {
                                    ?>
                                        <p> 
                                            <a class="a-lang" href="/<?= $lang ?>"><?= ucfirst($lang) ?></a>
                                        </p>
                                    <?php
                                }

                            ?>
                               
                      </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="site-description"><?php bloginfo('description') ?></div>
        <?php
    }
}


/* Menu */
if ( !function_exists( 'tutuweb_menu' ) ) {
    function tutuweb_menu($menu) {
        $menu = array(
            'theme_location' => $menu,
            'container' => 'nav',
            'container_class' => $menu,
            'items_wrap' => '<ul id="%1$s" class="%2$s sf-menu">%3$s</ul>'
        );
        wp_nav_menu( $menu );
    }
}

/* Pagination */
if ( !function_exists( 'tutuweb_pagination' )) {
    function tutuweb_pagination() {
        the_posts_pagination();
    }
}

/* Display thumbnail */
if( !function_exists ('tutuweb_thumbnail' )) {
    function tutuweb_thumbnail($size) {
        if( !is_single() && has_post_thumbnail() && !post_password_required() || has_post_format('image')) : ?>
            <figure class="post-thumbnail"> <?php the_post_thumbnail( $size ); ?></figure>
        <?php endif; ?>
    <?php }
}

/* Display title */
if( !function_exists ('tutuweb_entry_header') ) {
    function tutuweb_entry_header() { ?>
        <?php if( is_single() )  : ?> 
            <h1 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
        <?php else : ?>
            <h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
            <?php endif; ?>
    <?php }
}

/* Get data post */
if ( !function_exists ('tutuweb_entry_meta()') ) {
    function tutuweb_entry_meta() { ?>
        <?php if( !is_page() ) : ?>
            <div class="entry-meta">
                <?php
                    printf( __('<span class="author"> Posted by %1$s', 'tutuweb'),
                    get_the_author() );

                    printf( __('<span class="date-published"> at %1$s', 'tutuweb'),
                    get_the_modified_date() );

                    printf( __('<span class="category"> in %1$s ', 'tutuweb'),
                    get_the_category_list( ',' ) );

                    if( comments_open() ): 
                        echo '<span class="meta-reply">';
                            comments_popup_link(
                                __('Leave a comment', 'tutuweb'),
                                __('One comment', 'tutuweb'),
                                __('% comments'),
                                __('Read all comment', 'tutuweb'),
                            );
                            echo '</span>';
                    endif;
                ?>
            </div>
            <?php endif; ?>
    <?php }
}

/* Display content post */
if( !function_exists('tutuweb_entry_content')) {
    function tutuweb_entry_content() {
        if( !is_single() && !is_page() ) {
             print_r('1234');
            the_excerpt();
        } else {
            echo get_the_content();
            /* Paganation for post */

            $link_pages = array(
                'before' => __('<p>Page: ', 'tutuweb'),
                'after' => '</p>',
                'previouspagelink' => __('Previous Page', 'tutuweb'),
            );
            wp_link_pages( $link_pages );
        }
    }
}

/* Display content video */
function the_content_video($post_id=NULL) {
    if( !is_single() && !is_page() ) {
        $media = get_media_embedded_in_content(
            apply_filters( 'the_content', get_the_content() ), 'iframe'
        );
    
        printf( __(( $media[0] )) );
    } else {
        the_content();
            /* Paganation for post */

            $link_pages = array(
                'before' => __('<p>Page: ', 'tutuweb'),
                'after' => '</p>',
                'previouspagelink' => __('Previous Page', 'tutuweb'),
            );
        wp_link_pages( $link_pages );
    }
    
 }

/* ReadMore */
function tutuweb_readmore() {
    return '<a class="read-more" href="'. get_permalink( get_the_ID() ) . '">'.__('...[Read More]', 'tutuweb').'</a>';
}
add_filter('excerpt_more', 'tutuweb_readmore');


/* Displat tag */
if( !function_exists('tutuweb_entry_tag')) {
    function tutuweb_entry_tag() {
        if( has_tag() ) : 
            echo '<div class="entry-tag">';
            printf( __('Tagged in %1$s', 'tutuweb'), get_the_tag_list( '', ',' ) );
            echo '</div>';
        endif;
    }
}

/* Import css */

function tutuweb_style() {
    wp_register_style( 'main-style', get_template_directory_uri() ."/style.css", 'all' );
    wp_enqueue_style( 'main-style' );

    wp_register_style( 'reset-style', get_template_directory_uri() ."/reset.css", 'all' );
    wp_enqueue_style( 'reset-style' );

    /* Superfish Menu */
    wp_register_style( 'superfish-style', get_template_directory_uri() ."/css/superfish.css", 'all' );
    wp_enqueue_style( 'superfish-style' );

    wp_register_script( 'superfish-script', get_template_directory_uri() ."/js/superfish.js", array('jquery') );
    wp_enqueue_script( 'superfish-script' );

    /* Custom script */

    wp_register_script( 'custom-script', get_template_directory_uri() ."/custom.js", array('jquery') );
    wp_enqueue_script( 'custom-script' );

}

add_action('wp_enqueue_scripts', 'tutuweb_style');

/*
* Creating a function custom post type movies
*/


function tao_taxonomy() {
	/* Biến $label chứa các tham số thiết lập tên hiển thị của Taxonomy
	 */
	$labels = array(
		'name' => 'Các loại sản phẩm',
		'singular' => 'Loại sản phẩm',
		'menu_name' => 'Loại sản phẩm'
	);


	/* Biến $args khai báo các tham số trong custom taxonomy cần tạo
	 */
	$args = array(
		'labels'                     => $labels,
		'hierarchical'             => true,
		'public'                   => true,
		'show_ui'                   => true,
		'show_admin_column'       => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'            => true,
	);


	/* Hàm register_taxonomy để khởi tạo taxonomy
	 */
	register_taxonomy('loai-san-pham', '', $args);
    register_taxonomy( 'loai-san-pham', array( 'post', 'movies' ), $args );

}


// Hook into the ‘init’ action
add_action( 'init', 'tao_taxonomy', 0 );
  
function custom_post_type_movies() {
  
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Movies', 'Post Type General Name', 'tutuweb' ),
            'singular_name'       => _x( 'Movie', 'Post Type Singular Name', 'tutuweb' ),
            'menu_name'           => __( 'Movies', 'tutuweb' ),
            'parent_item_colon'   => __( 'Parent Movie', 'tutuweb' ),
            'all_items'           => __( 'All Movies', 'tutuweb' ),
            'view_item'           => __( 'View Movie', 'tutuweb' ),
            'add_new_item'        => __( 'Add New Movie', 'tutuweb' ),
            'add_new'             => __( 'Add New', 'tutuweb' ),
            'edit_item'           => __( 'Edit Movie', 'tutuweb' ),
            'update_item'         => __( 'Update Movie', 'tutuweb' ),
            'search_items'        => __( 'Search Movie', 'tutuweb' ),
            'not_found'           => __( 'Not Found', 'tutuweb' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'tutuweb' ),
        );
          
    // Set other options for Custom Post Type
          
        $args = array(
            'label'               => __( 'movies', 'tutuweb' ),
            'description'         => __( 'Movie news and reviews', 'tutuweb' ),
            'labels'              => $labels,
            // Features this CPT supports in Post Editor
            'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', 'post-formats' ),
            // You can associate this CPT with a taxonomy or custom taxonomy. 
            'taxonomies'          => array( 'movies', 'loai-san-pham' ),
            /* A hierarchical CPT is like Pages and can have
            * Parent and child items. A non-hierarchical CPT
            * is like Posts.
            */
            'hierarchical'        => false,
            'public'              => true,
            'show_ui'             => true,
            'show_in_menu'        => true,
            'show_in_nav_menus'   => true,
            'show_in_admin_bar'   => true,
            'menu_position'       => 5,
            'can_export'          => true,
            'has_archive'         => true,
            'exclude_from_search' => false,
            'publicly_queryable'  => true,
            'capability_type'     => 'post',
            'show_in_rest' => true,
            'menu_icon' => 'dashicons-youtube',
            'rewrite' => array( 'slug' => 'movies')
      
        );
          
        // Registering your Custom Post Type
        register_post_type( 'movies', $args );
      
    }
      
add_action( 'init', 'custom_post_type_movies', 0 );


function list_posts_by_taxonomy( $post_type, $taxonomy, $get_terms_args = array(), $wp_query_args = array() ){

    $tax_terms = get_terms( $taxonomy, $get_terms_args );

    if( $tax_terms ){

        foreach( $tax_terms  as $tax_term ){

            $query_args = array(

                'post_type' => $post_type,

                '$taxonomy' => $tax_term->slug,

                'post_status' => 'publish',

                'posts_per_page' => -1,

                'ignore_sticky_posts' => true

            );

            $query_args = wp_parse_args( $wp_query_args, $query_args );
            $my_query = new WP_Query( $query_args );

            if( $my_query->have_posts() ) { ?>

                <h2 id="<?php echo $tax_term->slug; ?>" class="tax_term-heading"><?php echo $tax_term->name; ?></h2>

                <ul>

                <?php while ($my_query->have_posts()) : $my_query->the_post(); ?>

                    <li><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>

                <?php endwhile; ?>

                </ul>

                <?php

            }

            wp_reset_query();

        }

    }

}



add_filter( 'toptal_saved_item_html', 'change_toptal_saved_item_html');
function change_toptal_saved_item_html( $inner_html_to_return ) {
	// Some custom code
	
	return $inner_html_to_return;
	
}