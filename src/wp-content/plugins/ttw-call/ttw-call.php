<?php 
/*
Plugin Name: TTW-Call
Plugin URI: https://flower2502.000webhostapp.com/
Description: Simple Call Plugin
Version: 1.0.0
Author: Tu Tu
Author URI: https://flower2502.000webhostapp.com/
License: GPLv2 or later
Text Domain: ttw_call
*/


define('TTW_CALL_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('TTW_CALL_PLUGIN_URL', plugin_dir_url(__FILE__));

add_action('admin_menu', 'setting_show_call');

function setting_show_call() {
    add_menu_page(
        "TTW Call",
        "TTW Call",
        "manage_options",
        "ttw-call",
        "ttw_setting_page",
        'dashicons-phone',

    );
}

function ttw_setting_page() {
    if ( isset($_POST["number_phone"]) ) {
        update_option("number_phone", $_POST["number_phone"]);
    }

    $number_phone = get_option("number_phone");

    require(TTW_CALL_PLUGIN_DIR . 'ttw-form-call-view.php');
}

add_action('wp_enqueue_scripts', function() {
    wp_register_style('ttw-call-css', TTW_CALL_PLUGIN_URL . 'style.css');
    wp_enqueue_style('ttw-call-css');
});

add_action('wp_footer', 'display_call');

function display_call() {
    $number_phone = get_option("number_phone");
    require(TTW_CALL_PLUGIN_DIR . 'ttw-call-view.php');
}