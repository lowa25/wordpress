<?php 
/*
Plugin Name: TTW-Contact-Form
Plugin URI: https://flower2502.000webhostapp.com/
Description: Simple Contact Form Plugin
Version: 1.0.0
Author: Tu Tu
Author URI: https://flower2502.000webhostapp.com/
License: GPLv2 or later
Text Domain: ttw_contact_form
*/

/* Register shortcode */
/* Form html */

define('TTW_CONTACT_FORM_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('TTW_CONTACT_FORM_PLUGIN_URL', plugin_dir_url(__FILE__));

add_shortcode('ttw_contact_form', 'show_form_contact');

add_action('wp_enqueue_scripts', function() {
    wp_register_style('ttw-form-css', TTW_CONTACT_FORM_PLUGIN_URL . 'style.css');
    wp_enqueue_style('ttw-form-css');
});


function show_form_contact() {
    ob_start();

    if( isset($_POST['submit']) ) {
        $name = isset($_POST['name']) ? $_POST['name'] : "";
        $phone = isset($_POST['phone']) ? $_POST['phone'] : "";
        $email = isset($_POST['email']) ? $_POST['email'] : "";
        $content = isset($_POST['content']) ? $_POST['content'] : "";

        if( $name == "" || $phone == "" || $content == "" ) {
            echo "<p class='error'>Please enter full information.</p>";
        } else {
            $to = get_option('admin_email');
            $subject = 'Contact New';
            $body = 'Name: $name <br> Phone: $phone <br> Email: $email <br> Content: $content';
            $headers = array('Content-Type: text/html; charset=UTF-8');

            wp_mail( $to, $subject, $body, $headers );
            echo "<p class='success'>Thank You!</p>";
        }
    }
    
    require(TTW_CONTACT_FORM_PLUGIN_DIR . 'form.php');

    return ob_get_clean();
}