<?php
/**
 * @package TTW-Weather
 */
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class TTW_Weather_Setting {
    protected $option;
    protected $option_group = 'ttw_weather_group';
    function __construct() {
        $this->option = get_option('ttw_weather_setting');
        // Add menu
        add_action('admin_menu', array($this, 'ttw_custom_submenu_page'));
        add_action('admin_init', array($this, 'register_setting'));
        add_action('admin_enqueue_scripts', function() {
            wp_register_script( 'ttw-js', TTW_WEATHER_PLUGIN_URL . "scripts/js/functions.js",  array('jquery'));
            wp_localize_script('ttw-js', 'ttw', [
                'url' => admin_url('admin-ajax.php'),
            ]);
            wp_enqueue_script('ttw-js');
        });

        add_action('wp_ajax_search_city_ajax', array($this, 'search_city_ajax'));
    }

    public function ttw_custom_submenu_page() {
        add_submenu_page(
            'options-general.php',
            'TTW Weather Settings',
            'TTW Settings',
            'manage_options',
            'ttw_weather',
            array($this, 'create_page'),
        );
    }

    public function create_page() {
        $option_group = $this->option_group;
        require(TTW_WEATHER_PLUGIN_DIR . 'views/ttw-weather-setting.php');
    }

    public function register_setting() {
        register_setting(
            $this->option_group,
            'ttw_weather_setting',
            [$this, 'save_setting']
        );
    }

    public function save_setting($input) {
        $new_input = [];
        if(isset ($input['city_name']) && !empty($input['city_name'])) {
            foreach ( $input['city_name'] as $value ) {
                $new_input['city_name'][] = preg_replace('/[ ]/u', '+', trim($value));
            }
        } else {
            $new_input['city_name'][] = 'Ho+Chi+Minh';
        }
        return $new_input;
    }

    public function search_city_ajax() {
        if(isset($_POST['city']) && !empty($_POST['city'])) {
            $data = TTW_Weather_API::request($_POST['city']);
            wp_send_json_success($data);
        }
    }
}