<?php
if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

class TTW_Weather {

    public function __construct() {
        $ttw_weather_widget = new TTW_Weather_Widget();
        $ttw_weather_setting = new TTW_Weather_Setting();
    }

    public function activation_hook() {

    }

    public function deactivation_hook() {

    }
}