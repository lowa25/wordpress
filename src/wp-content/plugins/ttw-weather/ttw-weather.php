<?php 
/*
Plugin Name: TTW-Weather
Plugin URI: https://flower2502.000webhostapp.com/
Description: Simple Weather Plugin
Version: 1.0.0
Author: Tu Tu
Author URI: https://flower2502.000webhostapp.com/
License: GPLv2 or later
Text Domain: ttw_weather
*/

if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

define('TTW_WEATHER_VERSION', '1.0.0');
define('TTW_WEATHER_MIIMUM_WP_VERSION', '4.1.1');
define('TTW_WEATHER_PLUGIN_URL', plugin_dir_url(__FILE__));
define('TTW_WEATHER_PLUGIN_DIR', plugin_dir_path(__FILE__));

require_once(TTW_WEATHER_PLUGIN_DIR . 'includes/class.ttw-weather-setting.php');
require_once(TTW_WEATHER_PLUGIN_DIR . 'includes/class.ttw-weather-api.php');
require_once(TTW_WEATHER_PLUGIN_DIR . 'includes/class.ttw-weather-widget.php');
require_once(TTW_WEATHER_PLUGIN_DIR . 'includes/class.ttw-weather.php');

$ttw_weather = new TTW_Weather();