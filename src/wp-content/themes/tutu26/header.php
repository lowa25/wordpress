<!DOCTYPE html>
  <html>
    <head>  
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
      <title>Scene & Succulents</title>

      <!-- Bootstrap -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">

       <!-- CSS  -->      
      <link href="<?php bloginfo('template_directory');?>/assets/css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
      <!-- Font Awesome -->
      <link href="<?php bloginfo('template_directory');?>/assets/css/font-awesome.css" rel="stylesheet">
      <!-- Skill Progress Bar -->
      <link href="<?php bloginfo('template_directory');?>/assets/css/pro-bars.css" rel="stylesheet" type="text/css" media="all" />
      <!-- Owl Carousel -->
      <link rel="stylesheet" href="<?php bloginfo('template_directory');?>/assets/css/owl.carousel.css">
      <!-- Default Theme CSS File-->
      <link id="switcher" href="<?php bloginfo('template_directory');?>/assets/css/themes/default-theme.css" type="text/css" rel="stylesheet" media="screen,projection"/>     
      <!-- Main css File -->
      <link href="<?php bloginfo('template_directory');?>/assets/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>

      <!-- Font -->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


    <?php wp_head(); ?>
    </head>
    <body>

    <?php //get_template_part('template-parts/preloader.php'); ?>
  
    <?php require 'template-pages/common/header.php'; ?>

    