<?php
// Add custom Theme Functions here

function custom_post_type_movies() {
  
    // Set UI labels for Custom Post Type
        $labels = array(
            'name'                => _x( 'Movies', 'Post Type General Name', 'flatsome-child' ),
            'singular_name'       => _x( 'Movie', 'Post Type Singular Name', 'flatsome-child' ),
            'menu_name'           => __( 'Movies', 'flatsome-child' ),
            'parent_item_colon'   => __( 'Parent Movie', 'flatsome-child' ),
            'all_items'           => __( 'All Movies', 'flatsome-child' ),
            'view_item'           => __( 'View Movie', 'flatsome-child' ),
            'add_new_item'        => __( 'Add New Movie', 'flatsome-child' ),
            'add_new'             => __( 'Add New', 'flatsome-child' ),
            'edit_item'           => __( 'Edit Movie', 'flatsome-child' ),
            'update_item'         => __( 'Update Movie', 'flatsome-child' ),
            'search_items'        => __( 'Search Movie', 'flatsome-child' ),
            'not_found'           => __( 'Not Found', 'flatsome-child' ),
            'not_found_in_trash'  => __( 'Not found in Trash', 'flatsome-child' ),
        );
          
    // Set other options for Custom Post Type
          
        $args = array(
            'label'               => __( 'movies', 'flatsome-child' ),
            'labels'              => $labels,
			'hierarchical'        => false,
			'description'         => 'description',
			'taxonomies'          => array('category', 'post_tag'),
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'menu_position'       => null,
			'menu_icon'           => 'dashicons-archive',
			'show_in_nav_menus'   => true,
			'show_in_rest'   => true,
			'publicly_queryable'  => true,
			'exclude_from_search' => false,
			'has_archive'         => true,
			'query_var'           => true,
			'can_export'          => true,
			'rewrite' => array( 'slug' => 'movies'),
			'capability_type'     => 'post',
			'supports'            => array(
				'title',
				'editor',
				'author',
				'thumbnail',
				'revisions',
				'post-formats',
			),
      
        );
          
        // Registering your Custom Post Type
        register_post_type( 'movies', $args );
      
}
      
add_action( 'init', 'custom_post_type_movies', 0 );