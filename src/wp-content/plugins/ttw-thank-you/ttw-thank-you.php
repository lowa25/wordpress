<?php 
/*
Plugin Name: TTW-Thank-You
Plugin URI: https://flower2502.000webhostapp.com/
Description: Simple Thank You Plugin
Version: 1.0.0
Author: Tu Tu
Author URI: https://flower2502.000webhostapp.com/
License: GPLv2 or later
Text Domain: ttw_thank_you
*/

add_filter('the_content', 'ttw_thank_you');

function ttw_thank_you($content) {
    if(is_single()) {
        $content = $content . "<p style='color: #663399'>Thank you for reading!</p>";
        return $content;
    }
}