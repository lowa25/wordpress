<?php

/**
 * Fired during plugin activation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Plugin_Camtu
 * @subpackage Plugin_Camtu/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Plugin_Camtu
 * @subpackage Plugin_Camtu/includes
 * @author     Your Name <email@example.com>
 */
class Plugin_Camtu_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
		$saved_page_args = array(
			'post_title'   => __( 'Saved - Cam Tu Plugin', 'toptal-save' ),
			'post_content' => '[toptal-saved]',
			'post_status'  => 'publish',
			'post_type'    => 'page'
		);
		// Insert the page and get its id.
		$saved_page_id = wp_insert_post( $saved_page_args );
		// Save page id to the database.
		add_option( 'toptal_save_saved_page_id', $saved_page_id );
	}

}
