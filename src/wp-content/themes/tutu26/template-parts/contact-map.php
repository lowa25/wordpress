<div class="footer-top">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="footer-top-inner">
                    <h2 class="title">Contact</h2>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                    <div class="contact">
                    <div class="row">
                        <div class="col s12 m6 l6">
                        <div class="contact-form">
                            <form>
                            <div class="input-field">
                                <input type="text" class="input-box" name="contactName" id="contact-name">
                                <label class="input-label" for="contact-name">Name</label>
                            </div>
                            <div class="input-field">
                                <input type="email" class="input-box" name="contactEmail" id="email">
                                <label class="input-label" for="email">Email</label>
                            </div>
                            <div class="input-field">
                                <input type="text" class="input-box" name="contactSubject" id="subject">
                                <label class="input-label" for="subject">Subject</label>
                            </div>
                            <div class="input-field textarea-input">
                                <textarea class="materialize-textarea" name="contactMessage" id="textarea1"></textarea>
                                <label class="input-label" for="textarea1">Message</label>
                            </div>
                            <button class="left waves-effect btn-flat brand-text submit-btn" type="submit">send message</button>
                            </form>
                        </div>
                        </div>
                        <div class="col s12 m6 l6">
                        <div class="contact-map">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.380622048141!2d106.703516850777!3d10.782132361997954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752f4eb52b593b%3A0xba0905d67375bb!2zNiBMw6ogVGjDoW5oIFTDtG4sIELhur9uIE5naMOpLCBRdeG6rW4gMSwgVGjDoG5oIHBo4buRIEjhu5MgQ2jDrSBNaW5oLCBWaeG7h3QgTmFt!5e0!3m2!1svi!2s!4v1654676187783!5m2!1svi!2s" width="100%" height="100%" frameborder="0" style="border:0"></iframe>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>