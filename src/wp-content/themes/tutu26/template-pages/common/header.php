
<header id="header" role="banner">
    <div class="navbar-fixed">
        <nav>
        <div class="container">
            <div class="nav-wrapper">

            <!-- LOGO -->

            <!-- TEXT BASED LOGO -->
            <a href="/" class="brand-logo">Scene & Succulents</a>

            <!-- Image Based Logo -->                
            
            <?php 
                $sideNav = get_menu_header('main_menu');

                if ( $sideNav !== false && count($sideNav) > 0){ 
                    echo '<ul class="right hide-on-med-and-down custom-nav menu-scroll">';
                    foreach ($sideNav as $index => $nav){
                        ?>
                            <li>
                                <a href="<?php echo $nav->url; ?>" ><span><?php echo $nav->title; ?></span></a>
                            </li>
                        <?php
                        
                    }
                    echo '</ul>';
                }
            ?>

            <!-- For Mobile View -->
            <?php 
                $sideNav = get_menu_header('main_menu');

                if ( $sideNav !== false && count($sideNav) > 0){ 
                    echo '<ul id="slide-out" class="side-nav menu-scroll">';
                    foreach ($sideNav as $index => $nav){
                        ?>
                            <li>
                                <a href="<?php echo $nav->url; ?>" ><span><?php echo $nav->title; ?></span></a>
                            </li>
                        <?php
                        
                    }
                    echo '</ul>';
                }
            ?>

            <a href="#" data-activates="slide-out" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
            </div>

        </div>
        </nav>
    </div>  
</header>