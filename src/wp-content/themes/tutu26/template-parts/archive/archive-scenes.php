<?php 
      $args = array(
        'post_type'   => 'scenes',
        'post_status' => 'publish',

        'order'               => 'DESC',
        'orderby'             => 'modified',

        'posts_per_page'         => 3,

        'no_found_rows'          => false,
        'cache_results'          => true,
        'update_post_term_cache' => true,
        'update_post_meta_cache' => true,
    );

    $query = new WP_Query( $args );

    $featured_image = IMG_PATH . '/banner-scene.jpeg'; 

    $postCategory = get_terms(array(
        'taxonomy' => 'scene_category',
        'hide_empty'  => false,
    ));

    print_r('11111');
?>

<div class="main-wrapper">
    <main role="main">
        <!-- START BLOG BANNER -->
        <section id="banner">
            <div class="parallax-container">
                <div class="parallax">
                    <img src="<?= $featured_image ?>">
                </div>
                <div class="overlay-header" style="background: url(<?= $featured_image; ?>); background-repeat: no-repeat; background-size: 100% 100%">       
                </div>
                <div class="overlay-content blog-head">
                    <div class="container">
                        <h1 class="header-title">Blog Scene List</h1>                  
                    </div>
                </div>
            </div>
        </section>

        <section id="blog-details">
            <div class="container">
                <div class="row">
                    <div class="col s12 m8 l8">
                    <?php if($query->have_posts()): ?>
                        <div class="blog-content blog-archive">
                        <?php 
                            while($query->have_posts()):
                                $query->the_post();
                                ?>
                                    <div class="blog-post">
                                        <div class="card">
                                            <div class="card-image">
                                                <img src="<?= get_the_post_thumbnail_url(); ?>">     
                                            </div>
                                            <div class="card-content blog-post-content">
                                                <h2><a href="<?php echo get_permalink(); ?>"><?= the_title(); ?></a></h2>
                                                <div class="meta-media">
                                                <div class="single-meta">
                                                    Post By <a href="#"><?= get_the_author(); ?></a>
                                                </div>
                                                <div class="single-meta">
                                                    Category : <a href="#">Web/Design</a>
                                                </div>
                                                </div>
                                                <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.</p>
                                            </div>
                                            <div class="card-action">
                                                <a href="#" class="post-comment"><i class="material-icons">comment</i><span>15</span></a>
                                                <a href="<?php echo get_permalink(); ?>" class="readmore-btn">Read More</a>
                                            </div>
                                        </div>
                                    </div>

                            <?php
                                endwhile;
                            ?>
                        </div>

                        <?php
                        wp_reset_query();
                        wp_reset_postdata();
                            endif;
                        ?>  
             
                        <!-- Start Pagination -->
                        <?= tutu26_pagination(max( 1, get_query_var( 'paged' ) ), $query->max_num_pages, array()); ?>
                </div>
                <!-- Start Sidebar -->
                    <?php get_template_part('template-parts/sidebar', '', array('post_type' => 'scenes', 'list_cate' =>  $postCategory )); ?>
                </div>
            </div>
        </section>     
        <!-- Start Footer -->
        <footer id="footer" role="contentinfo"> 
        
            <div class="footer-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col s12">
                            <div class="footer-inner">
                                <!-- Bottom to Up Btn -->
                                <button class="btn-floating btn-large up-btn"><i class="mdi-navigation-expand-less"></i></button>
                                <p class="design-info">Designed By <a href="http://www.markups.io/">MarkUps.io</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>        

    </main>
</div>
