<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
    <div class="entry-thumbnail">
        <?php tutuweb_thumbnail('thumbnail'); ?>
    </div>
    <div class="entry-header">
        <?php tutuweb_entry_header(); ?>
        <?php tutuweb_entry_meta(); ?>
    </div>
    <div class="entry-content">
        <?php tutuweb_entry_content(); ?>
        <?php //( is_single() ? tutuweb_entry_tag() : '' ); ?>
    </div>
</article>