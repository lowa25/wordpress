<?php 
      $args = array(
        'post_type'   => 'scenes',
        'post_status' => 'publish',

        'order'               => 'DESC',
        'orderby'             => 'modified',

        'posts_per_page'         => 3,

        'no_found_rows'          => false,
        'cache_results'          => true,
        'update_post_term_cache' => true,
        'update_post_meta_cache' => true,
    );

    $query = new WP_Query( $args );
?>

<section id="blog" class="scene-blog">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <div class="blog-inner">
                    <h2 class="title"> Scene Blog</h2>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.</p>
                <!-- Start Blog area -->
                <?php if($query->have_posts()): ?>
                <div class="blog-area">
                    <div class="row">
                    <!-- Start single blog post -->
                        <?php 
                            while($query->have_posts()):
                                $query->the_post();
                                ?>
                                    <div class="col s12 m4 l4">
                                        <div class="blog-post">
                                            <div class="card">
                                                <div class="card-image">
                                                    <img src="<?= get_the_post_thumbnail_url(); ?>" >
                                                </div>
                                                <div class="card-content blog-post-content">
                                                    <h2><a href="<?php echo get_permalink(); ?>"><?= the_title(); ?></a></h2>
                                                    <div class="meta-media">
                                                    <div class="single-meta">
                                                        Post By <a href="#"><?= get_the_author(); ?></a>
                                                    </div>
                                                    <div class="single-meta">
                                                        Category : <a href="#">Web/Design</a>
                                                    </div>
                                                    </div>
                                                    <p><?= get_field('title');?></p>
                                                </div>
                                                <div class="card-action">
                                                    <a class="post-comment" href="#"><i class="material-icons">comment</i><span>15</span></a>
                                                    <a class="readmore-btn" href="<?php echo get_permalink(); ?>">Read More</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php
                            
                            endwhile;
                        ?>
                    </div>

                    <!-- All Post Button -->
                    <a class="waves-effect waves-light btn-large allpost-btn" href="blog-archive.html">All Post</a>
                </div>         
                <?php
                        wp_reset_query();
                        wp_reset_postdata();
                    endif;
                ?>           
            </div>
        </div>
    </div>
</section>