<?php 
    $post_type = $args['post_type'];
    $category = $args['list_cate'];
?>
<div class="col s12 m4 l4">
    <aside class="sidebar">
    <!-- Start Single Sidebar -->
        <div class="single-sidebar">
            <h3>Recent News</h3>
            <!-- Single Recent News -->
            <div class="recent-news">
                <div class="recent-img">
                    <a href="blog-single.html"><img src="" alt="img"></a>
                </div>
                <div class="recent-body">
                    <h4><a href="blog-single.html">Recent News Title</a></h4>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                </div>
            </div>
            <!-- Single Recent News -->
            <div class="recent-news">
                <div class="recent-img">
                    <a href="blog-single.html"><img src="" alt="img"></a>
                </div>
                <div class="recent-body">
                    <h4><a href="blog-single.html">Recent News Title</a></h4>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                </div>
            </div>
            <!-- Single Recent News -->
            <div class="recent-news">
                <div class="recent-img">
                    <a href="blog-single.html"><img src="" alt="img"></a>
                </div>
                <div class="recent-body">
                    <h4><a href="blog-single.html">Recent News Title</a></h4>
                    <p>The point of using Lorem Ipsum is that it has a more-or-less normal.</p>
                </div>
            </div>
        </div>
        <!-- Start Single Sidebar -->
        <div class="single-sidebar">
            <h3>Categories</h3>
            <!-- Single Category -->
            
            <ul>
                <?php 
                    if($category) {
                        foreach($category as $key => $cate) {
                            // print_r($cate);
                            $term_link = get_term_link( $cate->term_id );
                            ?>       
                                <li class="cat-item"><a href="<?= $term_link; ?>"><?= $cate->name; ?></a></li>  
                            <?php
                        }
                    }
                ?>
            </ul>
        </div>
            <!-- Start Single Sidebar -->
        <div class="single-sidebar">
            <h3>Archives</h3>
            <!-- Single Category -->
            <ul class="archives">
            <li><a href="#">March 2015</a></li>
            <li><a href="#">April 2015</a></li>
            <li><a href="#">May 2015</a></li>
            <li><a href="#">June 2015</a></li>
            <li><a href="#">July 2015</a></li>
            </ul>
        </div>
        <!-- Start Single Sidebar -->
        <div class="single-sidebar">
            <h3>Tags</h3>
            <!-- Single Category -->
            <div class="tagcloud">
            <a href="#">Design</a>
            <a href="#">Photography</a>
            <a href="#">Development</a>
            <a href="#">Art</a>
            <a href="#">WordPress</a>
            <a href="#">Design</a>
            <a href="#">Photography</a>
            <a href="#">Development</a>                        
            <a href="#">WordPress</a>
            </div>
        </div>
            <!-- Start Single Sidebar -->
        <div class="single-sidebar">
            <h3>Important links</h3>                      
            <ul>
            <li><a href="#">Login</a></li>
            <li><a href="#">Link One</a></li>
            <li><a href="#">Link Two</a></li>
            <li><a href="#">Link Three</a></li>
            </ul>
        </div>

    </aside>
</div>